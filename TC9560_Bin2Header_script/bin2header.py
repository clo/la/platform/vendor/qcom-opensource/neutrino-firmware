#!/usr/bin/env python
import binascii

def bin_string_to_bin_value(input):
   highest_order = len(input) - 1
   result = 0
   for bit in input:
      result = result + int(bit) * pow(2,highest_order)
      highest_order = highest_order - 1
   return bin(result)

def hex_string_to_bin_string(input):
   lookup = {"0" : "0000", "1" : "0001", "2" : "0010", "3" : "0011", "4" : "0100", "5" : "0101", "6" : "0110", "7" : "0111", "8" : "1000", "9" : "1001", "A" : "1010", "B" : "1011", "C" : "1100", "D" : "1101", "E" : "1110", "F" : "1111"}
   result = ""
   for byte in input:
      result =  result + lookup[byte]
   return result
def hex_string_to_hex_value(input):
   bin_string = hex_string_to_bin_string(input)
   bin_value = bin_string_to_bin_value(bin_string)
   return hex(int(bin_value, 2))

def padded_hex(i, l):
    given_int = i
    given_len = l

    hex_result = hex(given_int)[2:] # remove '0x' from beginning of str
    num_hex_chars = len(hex_result)
    extra_zeros = '0' * (given_len - num_hex_chars) # may not get used..

    return ('0x' + hex_result if num_hex_chars == given_len else
            '?' * given_len if num_hex_chars > given_len else
            '0x' + extra_zeros + hex_result if num_hex_chars < given_len else
            None)

fb = open('hsic_fw.bin', 'rb')
fh = open('hsic_fw.h', 'w')

fh.write('/* hsic_fw.h */\n')
fh.write('/* =============================================================================\n')
fh.write(' *   COPYRIGHT @2015                                                            \n')
fh.write(' *                                                                              \n')
fh.write(' *   Toshiba America Electronic Components                                      \n')
fh.write(' *                                                                              \n')
fh.write(' *   ALL RIGHTS RESERVED.                                                       \n')
fh.write(' *   UNPUBLISHED ?PROTECTED UNDER COPYRIGHT LAWS.  USE OF A COPYRIGHT NOTICE    \n')
fh.write(' *   IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION OR DISCLOSURE.        \n')
fh.write(' *                                                                              \n')
fh.write(' *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF       \n')
fh.write(' *   TOSHIBA. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE        \n')
fh.write(' *   PRIOR EXPRESS WRITTEN PERMISSION OF TOSHIBA.                               \n')
fh.write(' *                                                                              \n')
fh.write(' *   PROJECT:   NEUTRINO                                                        \n')
fh.write(' *                                                                              \n')
fh.write(' *   VERSION:   V01_00020160412                                                 \n')
fh.write(' *                                                                              \n')
fh.write(' *   RELEASE:   Preliminary & Confidential                                      \n')
fh.write(' *   @date      4/12/2016  22:51                                                \n')
fh.write(' *                                                                              \n')
fh.write(' *   SAMPLE PROGRAMS ARE PROVIDED AS-IS WITH NO WARRANTY OF ANY KIND,           \n')
fh.write(' *   EITHER EXPRESS OR IMPLIED.                                                 \n')
fh.write(' *                                                                              \n')
fh.write(' *   TOSHIBA ASSUMES NO LIABILITY FOR CUSTOMERS\' PRODUCT DESIGN OR APPLICATIONS.\n')
fh.write(' *                                                                              \n')
fh.write(' *   THIS SOFTWARE IS PROVIDED AS-IS AND HAS NOT BEEN FULLY TESTED.  IT IS      \n')
fh.write(' *   INTENDED FOR REFERENCE USE ONLY.                                           \n')
fh.write(' *                                                                              \n')
fh.write(' *   TOSHIBA DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES AND ALL LIABILITY OR  \n')
fh.write(' *   ANY DAMAGES ASSOCIATED WITH YOUR USE OF THIS SOFTWARE.                     \n')
fh.write(' *                                                                              \n')
fh.write(' * ========================================================================== */\n')
fh.write('\n') 

fh.write('#define HAVE_FW_VERSION\n') 
fh.write('#ifdef  HAVE_FW_VERSION\n') 
fh.write('unsigned char fw_ver[] = "V01_00020160412";\n')
fh.write('unsigned char fw_name[] = "hsic_fw.h";\n')
fh.write('#endif\n')

fh.write('\n') 
fh.write('unsigned char fw_data[] = {\n')

i = 0
fh.write('    ')
while True:
    byte = fb.read(1)
    if not byte:
        print('File converted !!')
        break
    else:
        i += 1
        bin_value = bin_string_to_bin_value(byte)
        fh.write(padded_hex(int(bin_value, 2), 2))
        if(i == 16):
            fh.write(',\n    ')
        else:
            fh.write(', ')
        if(i == 16):
            i = 0

fh.write('\n') 
fh.write('};\n')

fb.close()
fh.close()






