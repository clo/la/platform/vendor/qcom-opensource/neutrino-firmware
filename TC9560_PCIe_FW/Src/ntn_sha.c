/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      25-Oct-2015 : Initial 
 */
 
/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/
#include "common.h"
#include "ntn_uart.h"

#undef NTN_SHA2_VALIDATION

#define NTN_SHA_REG_BASE										0x4000E000	

#define NTN_SHA_MODE_OFFS										0x0000
#define NTN_SHA_SZA_OFFS										0x0004
#define NTN_SHA_SZB_OFFS										0x0008
#define NTN_SHA_MSGLNA_OFFS									0x000C
#define NTN_SHA_MSGLNB_OFFS									0x0010
#define NTN_SHA_START_OFFS									0x001C

#define NTN_SHA_MSGIA_OFFS									0x0020
#define NTN_SHA_MSGIB_OFFS									0x0024
#define NTN_SHA_MSGIC_OFFS									0x0028
#define NTN_SHA_MSGID_OFFS									0x002C
#define NTN_SHA_MSGIE_OFFS									0x0030
#define NTN_SHA_MSGIF_OFFS									0x0034
#define NTN_SHA_MSGIG_OFFS									0x0038
#define NTN_SHA_MSGIH_OFFS									0x003C
#define NTN_SHA_MSGII_OFFS									0x0040
#define NTN_SHA_MSGIJ_OFFS									0x0044
#define NTN_SHA_MSGIK_OFFS									0x0048
#define NTN_SHA_MSGIL_OFFS									0x004C
#define NTN_SHA_MSGIM_OFFS									0x0050
#define NTN_SHA_MSGIN_OFFS									0x0054
#define NTN_SHA_MSGIO_OFFS									0x0058
#define NTN_SHA_MSGIP_OFFS									0x005C

#define NTN_SHA_STATUS_OFFS									0x0060
#define NTN_SHA_WRSTA_OFFS									0x0070
#define NTN_SHA_WRSTB_OFFS									0x0074
#define NTN_SHA_WRSTC_OFFS									0x0078
#define NTN_SHA_WRSTD_OFFS									0x007C
#define NTN_SHA_WRSTE_OFFS									0x0080
#define NTN_SHA_WRSTF_OFFS									0x0084
#define NTN_SHA_WRSTG_OFFS									0x0088
#define NTN_SHA_WRSTH_OFFS									0x008C

static unsigned int fw_size = 0;

static unsigned int NTN_SHA_read_fw_word()
{
	static int fw_cnt = 0;
	
	if (fw_cnt < fw_size) 
	{
		fw_cnt += 4;
		return (*(unsigned int *)(0x4000 + fw_cnt -4));
	}
	else 
	{
		return 0;
	}
}

void NTN_SHA_fw_calc(unsigned int *csum)
{
	int i;
	//unsigned int size;
	unsigned int loop;
	unsigned int status;
	
	//fw_size = sizeof(fw_data);
	//size = sizeof(fw_data) * 8;  // in bits
	fw_size = *(unsigned int *)(0x10000000 + 0x200 + 0xC);   /* size in bytes */
	
	hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MODE_OFFS, 0x04);
	
	hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_SZA_OFFS, 0x00);
	hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_SZB_OFFS,  fw_size * 8);
	hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGLNA_OFFS, 0x00);
	hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGLNB_OFFS, fw_size * 8);
	hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_START_OFFS, 0x04);
	
	loop = (fw_size * 8 + 127)/128;
	for (i = 0; i < loop; i++) /* loop for every 128 bits */
	{
		switch (i % 4)
		{
			case 0:
				/* 000 - 127 */
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIA_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIB_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIC_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGID_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_START_OFFS, 0x02);
				break;
			case 1:
				/* 128 - 255 */
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIE_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIF_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIG_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIH_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_START_OFFS, 0x12);
				break;
			case 2:
				/* 256 - 383 */
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGII_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIJ_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIK_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIL_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_START_OFFS, 0x22);
				break;
			case 3:
				/* 384 - 511 */
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIM_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIN_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIO_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MSGIP_OFFS, NTN_SHA_read_fw_word());
				hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_START_OFFS, 0x32);
				break;
			default:
				break;
		}
		
		if (( i && ((i+1) % 4) == 0) || ( i+1 == loop) )
		{
			hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_START_OFFS, 0x01);
			do {
				status = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_STATUS_OFFS);
			}while (status == 0x30);
		}
	}

	do {
		status = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_STATUS_OFFS);
	}while ((status & 0x1) != 0x1);
	for (i = 0; i< 10000; i++) ; 
	
	csum[0] = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_WRSTA_OFFS);
	csum[1] = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_WRSTB_OFFS);
	csum[2] = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_WRSTC_OFFS);
	csum[3] = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_WRSTD_OFFS);
	csum[4] = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_WRSTE_OFFS);
	csum[5] = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_WRSTF_OFFS);
	csum[6] = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_WRSTG_OFFS);
	csum[7] = hw_reg_read32 (NTN_SHA_REG_BASE, NTN_SHA_WRSTH_OFFS);
	
	return;
}

void NTN_SHA_reset(void)
{
	hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_MODE_OFFS, 0x04);
	hw_reg_write32 (NTN_SHA_REG_BASE, NTN_SHA_START_OFFS, 0x02);
	
	hw_reg_write32 (NTN_REG_BASE, 0x001008, 0x800);  //NRSTCTL MACEN=0
	hw_reg_write32 (NTN_REG_BASE, 0x001004, 0xFFFFFF);  //NCLKCTL MACEN=1 
	hw_reg_write32 (NTN_REG_BASE, 0x001008, 0x0);  //NRSTCTL MACEN=0
}



