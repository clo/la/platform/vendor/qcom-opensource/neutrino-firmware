#ifndef __NEUTRINO_H__
#define __NEUTRINO_H__

/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      25-Oct-2015 : Initial 
 */
#define NTN_FW_MAJ_VER 0x1
#define NTN_FW_MIN_VER 0x7

#define  NTN_M3_DBG_CNT_START           		0x0000C800  // Debugging count SRAM area start address
#define  NTN_M3_DBG_CNT_SIZE            		(19*4)      // Debugging count SRAM area size
/* NTN_M3_DBG_CNT_START + 4*0: 	DMA TX0
 *   .........................
 * NTN_M3_DBG_CNT_START + 4*4:  	DMA TX4
 * NTN_M3_DBG_CNT_START + 4*5:  	MAC LPI/PWR/EVENT
 * NTN_M3_DBG_CNT_START + 4*6:  	DMA RX0
 *   .........................
 * NTN_M3_DBG_CNT_START + 4*11:  DMA RX5
 * NTN_M3_DBG_CNT_START + 4*12:  TDM
 * NTN_M3_DBG_CNT_START + 4*13:  CAN
 * NTN_M3_DBG_CNT_START + 4*14:  FW version and capability
 * NTN_M3_DBG_CNT_START + 4*15:  M3_MS_Ticks (64 bits)
 * NTN_M3_DBG_CNT_START + 4*17:  INTC WDT Expiry Count
 * NTN_M3_DBG_CNT_START + 4*18:  Neutrino boot complete flags
 */
#define NTN_BOOT_COMPLETE 					  1
#define NTN_M3_IP_PARAMS_START                0x0000C900 //Store IP parameters like MAC address, ip address and link speed
#define NTN_M3_IP_PARAMS_SIZE                 176  //size 176 bytes

#define NTN_M3_DBG_RSVD_OFST		(4*14)
#define NTN_FW_IPA_EN_MASK     0x1
/*
  *  b7          b2               b1                     b0
  * -------------------------------------------------------
  * | Reserved    | IPA cap or not  | IPA offload en/dis  |  Byte 0
  * -------------------------------------------------------
  *
  *
  *  b7                      b4 b3                        b0
  * --------------------------------------------------------
  * |    FW Ver Maj Num       |      FW Ver Minor Num      |   Byte 1
  * --------------------------------------------------------
*/

#define NTN_REG_BASE										0x40000000
#define NTN_BOOT_MODE_STS_OFFSET                            0x4
#define NEUTRINO_IPA

#ifdef NEUTRINO_IPA

/* PCI Shared Region */
#define PCIE_REGION								0x60000000

/* EMAC Wrapper registers */
#define NTN_DMA_TXCHINTMASK02_REG_OFFS						0x3284
#define NTN_DMA_RXCHINTMASK00_REG_OFFS						0x3804

/* IPA uC */
#define IPA_TX_DOORBELL							0x0DC
#define IPA_RX_DOORBELL							0x0D8

#endif

/* EMAC registers */
#define NTN_EMAC_REG_BASE									0x4000A000
			
/* PCI registers */			
#define NTN_PCIE_REG_BASE									0x40020000
#define MSI_SEND_TRIGGER_OFFS								0x209C
			
/* M3 registers */
#define  CPU_REG_NVIC_NVIC        			  	(*((int *)(0xE000E004)))             /* Int Ctrl'er Type Reg.                */
#define  CPU_REG_NVIC_ST_CTRL     			  	(*((int *)(0xE000E010)))             /* SysTick Ctrl & Status Reg.           */
#define  CPU_REG_NVIC_ST_RELOAD   			  	(*((int *)(0xE000E014)))             /* SysTick Reload      Value Reg.       */
#define  CPU_REG_NVIC_ST_CURRENT  			  	(*((int *)(0xE000E018)))             /* SysTick Current     Value Reg.       */
#define  CPU_REG_NVIC_ST_CAL      			  	(*((int *)(0xE000E01C)))             /* SysTick Calibration Value Reg.       */
#define  CPU_REG_NVIC_SHPRI3      			  	(*((int *)(0xE000ED20)))             /* System Handlers 12 to 15 Prio.       */
#define  CPU_REG_NVIC_VTOR        			   	(*((int *)(0xE000ED08)))             /* Vect Tbl Offset Reg.                 */
			
/* Interrupt registers */			
#define NTN_INTC_REG_BASE								0x40008000
#define INTSTATUS_OFFS				    			    0x0000
#define GDMASTATUS_OFFS				    			  	0x0008
#define MACSTATUS_OFFS				    			    0x000C
#define TDMSTATUS_OFFS				    			    0x0010
#define EXTINTFLG_OFFS				    			    0x0014
#define PCIEL12FLG_OFFS				    			    0x0018
#define I2CSPIINTFLG_OFFS				  			    0x001C
#define INTMCUMASK0_OFFS				  			    0x0020
#define INTMCUMASK1_OFFS				  			    0x0024
#define INTMCUMASK2_OFFS				  			    0x0028
#define INTEXTMASK0_OFFS				  			    0x0030
#define INTEXTMASK1_OFFS				  			    0x0034
#define INTEXTMASK2_OFFS				  			    0x0038
#define INTINTXMASK0_OFFS				  			    0x0040
#define INTINTXMASK1_OFFS				  			    0x0044
#define INTINTXMASK2_OFFS				  			    0x0048
#define EXTINTCFG_OFFS				    			    0x004C
#define MCUFLG_OFFS				        			  	0x0054
#define EXT_FLG_OFFS				      			   	0x0058
#define INTINTWDCTL_OFFS				      			0x0060
#define INTINTWDEXP_OFFS				      			0x0064
#define INTINTWDMON_OFFS				      			0x0068

#define WDEXP_WDEXP_MAX_MASK  (0x0FFFFFFF)	//0xFFF_FFFF*16ns = 4.29sec
#define WDEXP_WDEXP_DEF_MASK  (0x03FFFFFF)	//0x3FF_FFFF*16ns = 1.07sec
#define WDEXP_WDEXP_100MS_MASK (0x01999999)	//0x199_9999*16ns = 100.0msec
#define WDCTL_WDEnable_MASK   (0x00000002)
#define WDCTL_WDRestart_MASK  (0x00000001)


/* SPI */			
#define NTN_SPI_REG_BASE										0x40005000	
#define NTN_SPIC_FlshMemMap0_OFFS						0x0000	
#define NTN_SPIC_FlshMemMap1_OFFS						0x0004
#define NTN_SPIC_DirAccCtrl0_OFFS						0x0008	
#define NTN_SPIC_DirAccCtrl1_OFFS						0x000C
#define NTN_SPIC_DirRdCtrl0_OFFS						0x0010	
#define NTN_SPIC_DirRdCtrl1_OFFS						0x0014	

#define NTN_SPIC_PrgAccCtrl0_OFFS						0x0400	
#define NTN_SPIC_PrgAccCtrl1_OFFS						0x0404
#define NTN_SPIC_PrgAccIntEn_OFFS						0x0408
#define NTN_SPIC_PrgAccStat_OFFS						0x040C

#define NTN_SPIC_PriBufDat_Start_OFFS				0x0500  /* 0x500 - 0x507 */
#define NTN_SPIC_SecBufDat_Start_OFFS				0x0600  /* 0x600 - 0x6FF */



typedef enum IRQn
{
/******  Cortex-M3 Specific Interrupt Numbers ***********************************************************************/
  INT_SRC_NMI          							= -14,       /* Non Maskable                                */
  INT_SRC_Hard_Fault               	= -13,       /* Cortex-M3 Hard Fault                        */     
  INT_SRC_Memory_Management        	= -12,       /* Cortex-M3 Memory Management                 */     
  INT_SRC_Bus_Fault                	= -11,       /* Cortex-M3 Bus Fault                         */     
  INT_SRC_Usage_Fault              	= -10,       /* Cortex-M3 Usage Fault                       */     
  INT_SRC_SVC                  			= -5,        /* Cortex-M3 SV Call                           */     
  INT_SRC_Debug_Monitor            	= -4,        /* Cortex-M3 Debug Monitor                     */     
  INT_SRC_Pend_SV                  	= -2,        /* Cortex-M3 Pend SV                           */
  SysTick_IRQn               				= -1,        /* Cortex-M3 System Tick                       */

/******  Neutrino Specific Interrupt Numbers *****************************************************************/
  INT_SRC_NBR_GPIO9               	= 0,         /*  Interrupt Pin 0.                                      */
  INT_SRC_NBR_GPIO10                = 1,         /*  Interrupt Pin 1.                                      */
  INT_SRC_NBR_GPIO11                = 2,         /*  Interrupt Pin 2.                                      */
  INT_SRC_NBR_EXT_INT               = 3,         /*  External Input Interrupt Pin: INTi                    */
  INT_SRC_NBR_I2C_SLAVE             = 4,         /*  I2C slave                                    */
  INT_SRC_NBR_I2C_MASTER            = 5,         /*  I2C mater                                    */
  INT_SRC_NBR_SPI_SLAVE             = 6,         /*  SPI slave                                    */
  INT_SRC_NBR_HSIC                  = 7,         /*  HSIC                                                  */
                                                      
	INT_SRC_NBR_MAC_LPI_EXIT          = 8,       	 /*  eMAC LPI exit                                         */
  INT_SRC_NBR_MAC_POWER             = 9,         /*  eMAC power managemenmt                                */
  INT_SRC_NBR_MAC_EVENTS            = 10,        /*  eMAC event from LPI, GRMII, Management counter...     */
	                                                  
  INT_SRC_NBR_EMACTXDMA0            = 11,        /*  eMAC Tx DMA Channel 3. VLAN Q1 traffic                */
  INT_SRC_NBR_EMACTXDMA1            = 12,        /*  eMAC Tx DMA Channel 4. VLAN Q2 traffic                */
  INT_SRC_NBR_EMACTXDMA2            = 13,        /*  eMAC Tx DMA Channel 3. VLAN Q1 traffic                */
  INT_SRC_NBR_EMACTXDMA3            = 14,        /*  eMAC Tx DMA Channel 4. VLAN Q2 traffic                */
  INT_SRC_NBR_EMACTXDMA4            = 15,        /*  eMAC Tx DMA Channel 4. VLAN Q2 traffic                */
                                                      
  INT_SRC_NBR_EMACRXDMA0            = 16,        /*  eMAC Rx DMA Channel 0. from Q0 all the other          */
  INT_SRC_NBR_EMACRXDMA1            = 17,        /*  eMAC Rx DMA Channel 1. from Q0 match M3 DA?           */
  INT_SRC_NBR_EMACRXDMA2            = 18,        /*  eMAC Rx DMA Channel 2. from Q0 match host DA?         */
  INT_SRC_NBR_EMACRXDMA3            = 19,        /*  eMAC Rx DMA Channel 3. from Q1 layer 2 gPTP           */
  INT_SRC_NBR_EMACRXDMA4            = 20,        /*  eMAC Rx DMA Channel 4. from Q2 untagged AV Control    */
  INT_SRC_NBR_EMACRXDMA5            = 21,        /*  eMAC Rx DMA Channel 5. from Q3 VLAN tagged            */
	
  INT_SRC_NBR_TDM0_IN_OV        		= 22,        /*  TMD Channel 0 input overflow.                         */
  INT_SRC_NBR_TDM1_IN_OV        		= 23,        /*  TMD Channel 1 input overflow.                         */
  INT_SRC_NBR_TDM2_IN_OV        		= 24,        /*  TMD Channel 2 input overflow.                         */
  INT_SRC_NBR_TDM3_IN_OV        		= 25,        /*  TMD Channel 3 input overflow.                         */
  INT_SRC_NBR_TDM_OUT_OV       		 	= 26,        /*  TMD Channel output overflow.                          */
  INT_SRC_NBR_TDM_OUT_UD        		= 27,        /*  TMD Channel output underflow.                         */
  INT_SRC_NBR_TDM_OUT_ERR       		= 28,        /*  TMD Channel output general error.                     */
 
  INT_SRC_NBR_QSPI                  = 29,        /*  qSPI interrupt.                                       */
  INT_SRC_NBR_GDMA0                 = 30,        /*  GDMA Channel 0.                                       */
  INT_SRC_NBR_GDMA1                 = 31,        /*  GDMA Channel 1.                                       */
  INT_SRC_NBR_GDMA2                 = 32,        /*  GDMA Channel 2.                                       */
  INT_SRC_NBR_GDMA3                 = 33,        /*  GDMA Channel 3.                                       */
  INT_SRC_NBR_GDMA4                 = 34,        /*  GDMA Channel 4.                                       */
  INT_SRC_NBR_GDMA5                 = 35,        /*  GDMA Channel 5.                                       */
  INT_SRC_NBR_GDMAGEN               = 36,        /*  GDMA General                                          */

  INT_SRC_NBR_SHA                   = 37,        /*  SHA.                                                  */
  INT_SRC_NBR_UART                  = 38,        /*  UART.                                                 */

  INT_SRC_NBR_CAN0_LINE0            = 39,        /*  CAN0.                                                 */
  INT_SRC_NBR_CAN0_LINE1            = 40,        /*  CAN1.                                                 */
  INT_SRC_NBR_PCIEC0                = 41,        /*  PCIe Controller Int0.                                 */
  INT_SRC_NBR_PCIEC1                = 42,        /*  PCIe Controller Int1.                                 */
  INT_SRC_NBR_PCIEC2                = 43,        /*  PCIe Controller Int2.                                 */
  INT_SRC_NBR_PCIE_L12              = 44,        /*                                                        */
  INT_SRC_NBR_MCU_FLAG              = 45,        /*                                                        */
  INT_SRC_NBR_CAN1_LINE0            = 46,        /*  CAN1 Line 0                                           */
  INT_SRC_NBR_CAN1_LINE1            = 47,        /*  CAN1 Line 1                                           */
  INT_SRC_NBR_WDT                   = 48,        /*  INTC WatchDog timer                                   */
	
} IRQn_Type;


/** Processor and Core Peripheral Section */

/* Configuration of the Cortex-M3 Processor and Core Peripherals */
#define __CM3_REV           	   0x0200    /* Cortex-M3 Core Revision                           */
#define __MPU_PRESENT          	   0         /* MPU present or not                                */
#define __NVIC_PRIO_BITS       	   3         /* Number of Bits used for Priority Levels           */
#define __Vendor_SysTickConfig 	   0         /* Set to 1 if different SysTick Config is used      */


#include "core_cm3.h"                       /* Cortex-M3 processor and core peripherals            */

#endif 

