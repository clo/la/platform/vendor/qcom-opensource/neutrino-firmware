#ifndef _COMMON_H__
#define _COMMON_H__

/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      22-Oct-2015 : Initial 
 */

#include "Neutrino.h"                       					/* Cortex-M3 processor and core peripherals            */
 
#define DEF_DISABLED									0
#define DEF_ENABLED										1

/* Change below to DEF_ENABLED to enable Wake-on-LAN (requires SW POR) */
#define NTN_M3POR_4PCIE_ENABLE   			DEF_ENABLED  	/* DEF_ENABLED to enable PCIE controller reset for suspend/resume */

/* Disable INT_SRC_NBR_MAC_EVENTS (it is a lot of), as the PCIe may not like to have too many MSI */
#define NTN_DISABLE_MAC_EVENTS_INT 		DEF_DISABLED

extern volatile unsigned long long m3Ticks;

static void delay(unsigned int ms)
{
	unsigned long long ticks = m3Ticks + ms;
	while (m3Ticks < ticks);
	return;	
}

static void hw_reg_write32 (unsigned int addr_base, unsigned int offset, unsigned int val)
{
	volatile unsigned int *addr = (volatile unsigned int *)(unsigned int *)(addr_base + offset);
	*addr = val;
}
static unsigned int hw_reg_read32 (unsigned int addr_base, unsigned int offset)
{
	volatile unsigned int *addr = (volatile unsigned int *)(unsigned int *)(addr_base + offset);
	return (*addr);
}

#if ( NTN_M3POR_4PCIE_ENABLE == DEF_ENABLED )
void NTN_POR_init_pcie(void);
#endif

void NTN_SPI_init(void);
unsigned int NTN_SPI_read_mfid(void);
unsigned int NTN_SPI_read_status(void);
int NTN_SPI_read_word(unsigned int phy_addr);
void NTN_SPI_fast_read(unsigned int phy_addr, unsigned int *data);
int NTN_SPI_prog_page(unsigned int phy_addr, unsigned char *data);
int NTN_SPI_erase_sector(unsigned int phy_addr);
int NTN_SPI_erase_32k_block(unsigned int phy_addr);
int NTN_SPI_erase_64k_block(unsigned int phy_addr);
int NTN_SPI_erase_chip(void);

int NTN_SPI_write_status(unsigned int status);
unsigned int NTN_QSPI_read_mfid(void);

int NTN_SPI_test_main (void);
int NTN_QSPI_test_main(void);

void NTN_SHA_fw_calc(unsigned int *csum);
			
#endif //_COMMON_H__

