/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      5-Feb-2016 : Initial 
 */

#include "common.h"
#include "neutrino.h"

#define NTN_M3_MASK_INT_IN_MSIGEN 	

extern void NTN_PCIe_ISR_Handler(int num);

void NTN_MSIGEN_ISR_Handler(int num)
{  
#ifdef NTN_M3_MASK_INT_IN_MSIGEN
	unsigned int mask_val;
#endif
	volatile unsigned int ipa_en = 0;

	ipa_en =  *(unsigned int *)(NTN_M3_DBG_CNT_START + NTN_M3_DBG_RSVD_OFST);
	ipa_en = ipa_en & NTN_FW_IPA_EN_MASK;

#ifdef NTN_M3_MASK_INT_IN_MSIGEN
	mask_val   = hw_reg_read32(NTN_INTC_REG_BASE, INTMCUMASK1_OFFS);
	/* ****************************************************** *
	 * MAC:   	bit 08 - 10                                   *
	 * DMA:     bit 11 - 21                                   *
	 * TDM:     bit 22 - 28                                   *
	 * ****************************************************** */
	if(ipa_en)
		mask_val |= 0x1FFEDF00;
	else
		mask_val |= 0x1FFFFF00;

	hw_reg_write32(NTN_INTC_REG_BASE, INTMCUMASK1_OFFS, mask_val);

	mask_val   = hw_reg_read32(NTN_INTC_REG_BASE, INTMCUMASK2_OFFS);
	/* ****************************************************** *
	 * CAN0:   	bit 07 - 08                                   *
	 * CAN1:    bit 13 - 14                                   *
   * WDT :    bit 15 (Ignore. Only Increment Counter now)   *
	 * ****************************************************** */
	mask_val |= 0x00006180;             
	hw_reg_write32(NTN_INTC_REG_BASE, INTMCUMASK2_OFFS, mask_val);
#endif
	
	/* MSI GEN */
	hw_reg_write32(NTN_PCIE_REG_BASE, MSI_SEND_TRIGGER_OFFS, 0 /* MSI Number */);
	
	return;
}


/* ****************************************************************************** *
 *                eMAC                                                            *
 * ****************************************************************************** */
void MAC_LPI_EXIT_IRQHandle (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_MAC_LPI_EXIT);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 5*4) += 1;
}

void MAC_POWER_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_MAC_POWER);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 5*4) += 1;
}

void MAC_EVENTS_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_MAC_EVENTS);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 5*4) += 1;
}

/* ****************************************************************************** *
 *                DMATX CH                                                        *
 * ****************************************************************************** */
void EMACTXDMA0_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACTXDMA0);
	*(unsigned int *)(NTN_M3_DBG_CNT_START +  0 * 4) += 1;
}

void EMACTXDMA1_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACTXDMA1);
	*(unsigned int *)(NTN_M3_DBG_CNT_START +  1 * 4) += 1;
}

void EMACTXDMA2_IRQHandler (void)
{
	volatile unsigned int ipa_en = 0;

	ipa_en =  *(unsigned int *)(NTN_M3_DBG_CNT_START + NTN_M3_DBG_RSVD_OFST);
	ipa_en = ipa_en & NTN_FW_IPA_EN_MASK;

	if (ipa_en) {
		static unsigned long int count = 0;
		volatile unsigned int *addr = (volatile unsigned int *)(PCIE_REGION + IPA_TX_DOORBELL);
		++count;

		/* Mask all TX Interrupts */
		hw_reg_write32(NTN_REG_BASE, NTN_DMA_TXCHINTMASK02_REG_OFFS, 0x00);
		*addr = count;
	} else
		NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACTXDMA2);

	*(unsigned int *)(NTN_M3_DBG_CNT_START +  2 * 4) += 1;
}

void EMACTXDMA3_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACTXDMA3);
	*(unsigned int *)(NTN_M3_DBG_CNT_START +  3 * 4) += 1;
}

void EMACTXDMA4_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACTXDMA4);
	*(unsigned int *)(NTN_M3_DBG_CNT_START +  4 * 4) += 1;
}

/* ****************************************************************************** *
 *                DMARX CH                                                        *
 * ****************************************************************************** */
void EMACRXDMA0_IRQHandler (void)
{
	volatile unsigned int ipa_en = 0;

	ipa_en =  *(unsigned int *)(NTN_M3_DBG_CNT_START + NTN_M3_DBG_RSVD_OFST);
	ipa_en = ipa_en & NTN_FW_IPA_EN_MASK;

	if (ipa_en) {
		static unsigned long int count = 0;
		volatile unsigned int *addr = (volatile unsigned int *)(PCIE_REGION + IPA_RX_DOORBELL);
		++count;

		/* Mask all RX Interrupts */
		hw_reg_write32(NTN_REG_BASE, NTN_DMA_RXCHINTMASK00_REG_OFFS, 0x00);
		*addr = count;
	} else
		NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACRXDMA0);

	*(unsigned int *)(NTN_M3_DBG_CNT_START +  6 * 4) += 1;
}

void EMACRXDMA1_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACRXDMA1);
	*(unsigned int *)(NTN_M3_DBG_CNT_START +  7 * 4) += 1;
}

void EMACRXDMA2_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACRXDMA2);
	*(unsigned int *)(NTN_M3_DBG_CNT_START +  8 * 4) += 1;
}

void EMACRXDMA3_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACRXDMA3);
	*(unsigned int *)(NTN_M3_DBG_CNT_START +  9 * 4) += 1;
}

void EMACRXDMA4_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACRXDMA4);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 10 * 4) += 1;
}

void EMACRXDMA5_IRQHandler (void)
{
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_EMACRXDMA5);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 11 * 4) += 1;
}

/* ****************************************************************************** *
 *                TDM                                                             *
 * ****************************************************************************** */
void TDM0_IN_OV_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_TDM0_IN_OV);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 12 * 4) += 1;
}

void TDM1_IN_OV_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_TDM1_IN_OV);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 12 * 4) += 1;	
}

void TDM2_IN_OV_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_TDM2_IN_OV);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 12 * 4) += 1;	
}

void TDM3_IN_OV_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_TDM3_IN_OV);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 12 * 4) += 1;	
}

void TDM_OUT_OV_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_TDM_OUT_OV);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 12 * 4) += 1;	
}

void TDM_OUT_UD_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_TDM_OUT_UD);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 12 * 4) += 1;	
}

void TDM_OUT_ERR_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_TDM_OUT_ERR);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 12 * 4) += 1;	
}       

/* ****************************************************************************** *
 *                CAN                                                             *
 * ****************************************************************************** */
void CAN0Ln0_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_CAN0_LINE0);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 13 * 4) += 1;
}

void CAN0Ln1_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_CAN0_LINE1);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 13 * 4) += 1;
}

void CAN1Ln0_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_CAN1_LINE0);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 13 * 4) += 1;
}

void CAN1Ln1_IRQHandler (void)
{	
	NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_CAN1_LINE1);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 13 * 4) += 1;
}

void WDT_IRQHandler (void)
{
	int  data;	
	//NTN_MSIGEN_ISR_Handler(INT_SRC_NBR_WDT);
	*(unsigned int *)(NTN_M3_DBG_CNT_START + 17 * 4) += 1;
	data = hw_reg_read32(NTN_INTC_REG_BASE, INTINTWDCTL_OFFS);
	hw_reg_write32(NTN_INTC_REG_BASE, INTINTWDCTL_OFFS, (data | (WDCTL_WDRestart_MASK)));
}

/* ****************************************************************************** *
 *                PCIE                                                            *
 * ****************************************************************************** */

void PCIEC0_IRQHandler (void)
{
	NTN_PCIe_ISR_Handler(INT_SRC_NBR_PCIEC0);
}

void PCIEC1_IRQHandler (void)
{
	NTN_PCIe_ISR_Handler(INT_SRC_NBR_PCIEC1);
}

void PCIEC2_IRQHandler (void)
{
	NTN_PCIe_ISR_Handler(INT_SRC_NBR_PCIEC2);
}

void PCIE_L12_IRQHandler (void)
{
	NTN_PCIe_ISR_Handler(INT_SRC_NBR_PCIE_L12);
}
