/* ============================================================================
 *   COPYRIGHT © 2015
 *
 *   Toshiba America Electronic Components
 *
 *   ALL RIGHTS RESERVED.
 *   UNPUBLISHED – PROTECTED UNDER COPYRIGHT LAWS.  USE OF A COPYRIGHT NOTICE
 *   IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   TOSHIBA. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE
 *   PRIOR EXPRESS WRITTEN PERMISSION OF TOSHIBA.
 *
 *   PROJECT:   NEUTRINO
 *
 *   VERSION:   Revision: 1.0
 *
 *   RELEASE:   Preliminary & Confidential
 *   @date      25 September 2015
 *
 *   EXAMPLE PROGRAMS ARE PROVIDED AS-IS WITH NO WARRANTY OF ANY KIND, 
 *   EITHER EXPRESS OR IMPLIED.
 *
 *   TOSHIBA ASSUMES NO LIABILITY FOR CUSTOMERS' PRODUCT DESIGN OR APPLICATIONS.
 *   
 *   THIS SOFTWARE IS PROVIDED AS-IS AND HAS NOT BEEN FULLY TESTED.  IT IS
 *   INTENDED FOR REFERENCE USE ONLY.
 *   
 *   TOSHIBA DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES AND ALL LIABILITY OR
 *   ANY DAMAGES ASSOCIATED WITH YOUR USE OF THIS SOFTWARE.
 *
 * ========================================================================= */
 
/*
 *********************************************************************************************************
 *
 * Filename      : ntn_gpio.c
 * Programmer(s) : WZ
 *                 
 *********************************************************************************************************
 */
 
#include "ntn_gpio.h"

void taec_gpio0_config_output( unsigned int data)
{
	unsigned int config; 
	
	config = ~(data);
	NTN_GPIO_INPUT_ENABLE0   = config;    // config as output
}

void taec_gpio0_output_data(unsigned int data)
{
  NTN_GPIO_OUTPUT0     = data;    // drive 1 on all gpio pin[31:0]  
}


void taec_gpio1_config_output( unsigned int data)
{
	unsigned int config; 
	
	NTN_GPIO_INPUT_ENABLE1   = config;    // config as output
}

void taec_gpio1_output_data(unsigned int data)
{
  NTN_GPIO_OUTPUT1     = data;    // drive 1 on all gpio pin[31:0]  
}
